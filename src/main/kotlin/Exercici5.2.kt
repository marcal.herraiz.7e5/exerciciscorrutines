import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    runBlocking {
        launch {
            corroutine11()
        }
        launch {
            corroutine22()
        }
    }
    println("Completed")
}

suspend fun corroutine11() = coroutineScope {
    launch{
        println("Hello World 1.1")
        delay(3000)
        println("Hello World 1.2")
        delay(3000)
        println("Hello World 1.3")
    }
}

suspend fun corroutine22() = coroutineScope {
    launch{
        println("Hello World 2.1")
        delay(2000)
        println("Hello World 2.2")
        delay(2000)
        println("Hello World 2.3")
    }
}

/*
* LA SORTIDA DEL CODI ES LA SEGÜENT:
*
* Hello World 1.1
* Hello World 2.1
* Hello World 2.2
* Hello World 1.2
* Hello World 2.3
* Hello World 1.3
* Completed
* */