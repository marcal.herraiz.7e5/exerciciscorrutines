import kotlinx.coroutines.*

@OptIn(DelicateCoroutinesApi::class)
fun main() {
    println("The main program is started")
    GlobalScope.launch {
        doBackground()
    }
    println("The main program continues")
    runBlocking {
        delay(1000)
        println("The main program is finished")
    }
}

suspend fun doBackground() {
    println("Background processing started")
    delay(1500)
    println("Background processing finished")
}

/*
* LA SORTIDA DEL CODI ES LA SEGÜENT:
*
* The main program is started
* The main program continues
* Background processing started
* The main program is finished
* */