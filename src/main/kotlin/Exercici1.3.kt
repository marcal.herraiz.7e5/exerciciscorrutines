import kotlinx.coroutines.*

suspend fun main() {
    println("The main program is started")
    withContext(Dispatchers.Default) {
        doBackgroundAgain()
    }
    println("The main program continues")
    runBlocking {
        delay(1000)
        println("The main program is finished")
    }
}

suspend fun doBackgroundAgain() {
    println("Background processing started")
    delay(1500)
    println("Background processing finished")
}

/*
* LA SORTIDA DEL CODI ES LA SEGÜENT:
*
* The main program is started
* Background processing started
* Background processing finished
* The main program continues
* The main program is finished
* */