import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.system.exitProcess

fun main() {

    println("You have 10 seconds to guess the secret number...")
    val randomNumber = (1..5).random()

    startTimer()

    do {
        print("Enter a number: ")
        val guessNumber = readln().toInt()
        if (guessNumber!=randomNumber){
            println("Wrong number!")
        }else{
            println("You got it!")
        }
    }while (guessNumber!=randomNumber)

}

@OptIn(DelicateCoroutinesApi::class)
fun startTimer() {
    GlobalScope.launch {
        delay(10000)
        println("\nThe time is up!")
        exitProcess(0)
    }
}
