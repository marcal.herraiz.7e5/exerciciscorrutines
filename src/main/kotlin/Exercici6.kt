import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

enum class Colors(val colorCode: String) {
    YELLOW("\u001B[33m"),
    MAGENTA("\u001B[35m"),
    CYAN("\u001B[36m"),
}

fun main() {
    println("Start of The Race\n")
    runBlocking {
        launch {
            racingHorse("Seabiscuit", Colors.YELLOW)
        }
        launch {
            racingHorse("Man o' War", Colors.MAGENTA)
        }
        launch {
            racingHorse("Phar Lap", Colors.CYAN)
        }
    }
}

suspend fun racingHorse(name: String, color: Colors) {
    for (i in 1..4) {
        val randomDelay: Long = (500..1000).random().toLong()
        delay(randomDelay)
        println("${color.colorCode}$name reaches sector $i")
    }
    println("${color.colorCode}$name finished the race \uD83C\uDFC1\uD83C\uDFC1\uD83C\uDFC1")
}
