import kotlinx.coroutines.*

@OptIn(DelicateCoroutinesApi::class)
fun main() {
    println("The main program is started")
    GlobalScope.launch {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

/*
* LA SORTIDA DEL CODI ES LA SEGÜENT:
*
* The main program is started
* The main program continues
* Background processing started
* Background processing finished
* The main program is finished
* */