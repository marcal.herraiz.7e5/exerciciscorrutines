import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.system.measureTimeMillis

fun main() {
    val song = File("canço.txt").readLines()

    val time = reproduceSong(song)

    val minutes = (time / 60000)
    val seconds = ((time % 60000) / 1000)
    println("\nTemps total $minutes:$seconds")
}

fun reproduceSong(song: List<String>): Long {
    val time = measureTimeMillis {
        runBlocking {
            for(i in 0..song.lastIndex){
                println(song[i])
                delay(3000)
            }
        }
    }
    return time
}
