import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    runBlocking {
        corroutine1()
        corroutine2()
    }
    println("Completed")
}

suspend fun corroutine1() = coroutineScope {
    launch{
        println("Hello World 1.1")
        delay(3000)
        println("Hello World 1.2")
        delay(3000)
        println("Hello World 1.3")
    }
}

suspend fun corroutine2() = coroutineScope {
    launch{
        println("Hello World 2.1")
        delay(2000)
        println("Hello World 2.2")
        delay(2000)
        println("Hello World 2.3")
    }
}




/*
* En aquest codi el que està passant és que primer executa una corrutina i després la següent
*
* LA SORTIDA DEL CODI ES LA SEGÜENT:
*
* Hello World 1.1
* Hello World 1.2
* Hello World 1.3
* Hello World 2.1
* Hello World 2.2
* Hello World 2.3
* Completed
* */