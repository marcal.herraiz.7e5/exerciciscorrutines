import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    launch {
        printMessage(3)
    }
    println("Finished")
}

suspend fun printMessage(i: Int) {
    for (j in 1..i){
        println("$j Hello World")
        delay(100)
    }
}

/*
* LA SORTIDA DEL CODI ES LA SEGÜENT:
*
* Finished
* 1 Hello World
* 2 Hello World
* 3 Hello World
* */